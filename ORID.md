
- Draw Concept Map as Mind Map.
- I feel confused. 
- Concept Map is a Process Oriented Tool, but Mind Map is a Result Oriented Tool. So, when we learn a new thing, we can use Concept Map to update our knowledge network. Otherwise, when we want to comb through the knowledge we learned, we can use Mind Map which help us well.
- Next time, we should focus more on how concepts relate to each other.